= gettext_html_auto.js : traduction de WebExtension
:slug: gettext_html_auto_translation_of_webextension_pages
:lang: fr
:date: 2020-05-25
:author: Siltaar

La méthode officielle pour traduire une WebExtension comme Meta-Press.es est
d'utiliser l'API `i18n`. Malheureusement cette dernière impose de définir
soi-même une clé pour chaque portion de texte à traduire, or l'exercice se
révèle chronophage, tout particulièrement quand il s'agit de grosse page web
comportant plein de petits textes comme les formulaires.

C'est le cas de l'interface principale de Meta-Press.es, ainsi que de sa page
de préférences. C'est d'ailleurs le cas de beaucoup de pages de préférences de
WebExtensions.

Pour me rapprocher du fonctionnement plus simple du standard GNU `gettext`, j'ai décidé d'utiliser l'implémentation https://github.com/guillaumepotier/gettext.js/[gettext.js] de Guillaume Potier, et de coder les fonction requise pour :

- collecter les textes à traduire (`xgettext`),
- maintenir un fichier `template.json` ainsi que les traductions correspondantes,
- et traduire les pages HTML lors de leur chargement.

Comme annoncé précédemment, Meta-Press.es est désormais disponible en anglais,
en français, en espagnol et en espéranto !

Si vous souhaitez contribuer une nouvelle langue, il suffit de remplir les
blancs du fichier
https://framagit.org/Siltaar/meta-press-ext/-/blob/master/html_locales/template.json[templace.json]
et de proposer votre nouvelle traduction via une _pull-request_ (ou de
m'envoyer le fichier par courriel).

Si vous cherchez une manière simple de traduire (internationaliser) votre
_Addon_ Firefox, ou au moins sa page de préférences, vous pouvez utiliser ce
code, que j'ai publié dans le dépôt séparé
https://framagit.org/Siltaar/gettext_html_auto.js[gettext_html_auto.js] avec
sa documentation.

Avec https://framagit.org/Siltaar/month_nb[month_nb], c'est le 2e outil
réutilisable construit pour Meta-Press.es.
