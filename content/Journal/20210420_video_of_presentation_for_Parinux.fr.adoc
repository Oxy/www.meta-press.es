= Vid&eacute;o de la pr&eacute;sentation de Meta-Press.es pour Parinux
:slug: 2021_video_presentation_of_meta-press.es_for_Parinux
:lang: fr
:date: 2021-04-20
:author: Siltaar

L'association Parinux a mis en ligne la vidéo de la présentation de
Meta-Press.es réalisée lors de la « Soirée Conversation autour du Libre » du
jeudi 15 avril 2021.

https://tube.fdn.fr/videos/watch/9ae43c36-3dc8-4d85-844b-c5a5335d77ed[*Meta-Press.es
: moteur de recherche décentralisé, écologique et protégeant la vie privée, par
Simon D.*] (1h 28min 24sec)

Plus de 160 personnes ont assisté aux présentations de Meta-Press.es cette
année et il y a eu 600 téléchargements dans les 90 derniers jours.

Au passage une présentation aura encore lieu le *mercredi 21 avril 2021 à
18h00* à l'initiative de https://montpellibre.fr/[Montpel'Libre] via Meet
Jit.si.  https://meet.jit.si/webinaire_montpellibre_vienumerique[*Cliquez ici
pour rejoindre l'évènement*].
https://montpellibre.fr/spip.php?article5141[Cliquez ici pour plus
d'information…]
