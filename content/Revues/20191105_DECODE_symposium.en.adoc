= [DecodeProject.eu] Exhibitors and Tech talk : Meta-Press.es
:slug: decode_project_eu_3rd_symopsium
:lang: en
:date: 2019-11-05
:author: Siltaar

https://decodeproject.eu/events/exhibitors[*Exhibitors and Tech talk : Meta-Press.es*]

Explore the press from your computer using Meta-Press.es, with no
middlemen between the newspapers and you. Discover millions of results
within seconds, and explore the ten last ones of each newspaper in
Firefox via this addon. You can also filter results (by newspaper, date
or a sub-query), or select some to create a press review and export
it in RSS format.

Meta-Press.es is a free software project building an alternative to
Google News for journalists and associations. It's developped by Simon
Descarpentries (member of the French net neutrality defense association
La Quadrature du Net and web artisan) with already a few contributers. It
have been backed by the Wau Holland Foundation. Any kind of contribution
will be warmly welcome.

https://decodeproject.eu/events/exhibitors
