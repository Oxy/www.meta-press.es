= [JDEV 2020] Keynote : Meta-press.es
:slug: 20200710_JDEV_2020
:lang: fr
:date: 2020-07-10
:author: Siltaar

Meta-Press.es est une extension Firefox pour faire sa revue de presse
sans intermédiaire, avec une base croissante contenant plus de 285
sources (de 55 pays et en 23 langues). L'objectif et les motivations de
Meta-Press.es seront présentés avant de se pencher sur une
démonstration d'utilisation. L'éco-système des addons Firefox et
l'architecture de Meta-Press.es sont également abordés.

Un détournement de l'outil pour des recherches dans les journaux
scientifiques et dans les VREs est également mentionné et fait l'objet
d'un appel à contribution. Ce sera un clin d'oeil au mouvement de la
Science Ouverte.

https://www.canal-u.tv/video/jdev/jdev2020_keynote_meta_press_es.58013[*JDev2020 : Keynote : Meta-press.es*] (vidéo)

http://devlog.cnrs.fr/jdev2020/posters#t2[Poster du projet] (PDF)

Transcription de l'introduction :

Meta-press.es est là pour répondre aux problèmes posés par la position dominante
des moteurs de recherches sur le web.

En effet, en centralisant à grande échelle un service donné, ces derniers se
retrouvent dotés d'un grand pouvoir auprès de leurs utilisateurs. Ils peuvent
censurer certains résultats s'il le souhaitent (comme le fait Google en Chine)
voire plus subtilement décider arbitrairement de quels résultats mettre en
avant.

Aujourd'hui par exemple Wikipedia survit principalement grâce à Google, non pas
parce que ce dernier ferait de gros dons à l'encyclopédie, mais parce que son
algorithme de tri des résultats favorise nettement Wikipedia. Il est en effet
assez rare de ne pas trouver un résultats Wikipedia dès la 1ère page d'une
recherche sur Google, ce qui maintient l'encyclopédie participative sous
respirateur.

Si, du jour au lendemain, Google reléguait Wikipedia derrière l'encyclopédie
Hachette en ligne, qui penserait encore à faire un don à Noël pour soutenir la
belle œuvre ?

Et je ne parle pas d'influencer des élections là… (mais d'autres y pensent).

Le besoin auquel répond Meta-Press.es, c'est donc d'établir un moteur de
recherche qui ne soit pas contrôlé par un acteur unique. J'ai cherché, aux
prémisses de Meta-Press.es (en 2013), le moyen d'assembler un index réparti
(comme l'ont fait avant moi des projets tels que YaCy ou Searx…). « Que chacun
indexe son contenu, et les résultats seront bien gardés » ou encore : mieux
vaut demander à ceux qui savent…

Étant bénévole à la revue de presse de la Quadrature du Net, je me suis vite
rendu compte que les journaux en ligne indexaient honnêtement leur contenus et
que dans ce cas de figure précis, c'est l'actualité d'un résultat (sa date) qui
fait sa pertinence. Or pour trier des résultats par date, il n'y a pas besoin
d'être Google, votre ordinateur et même votre téléphone portable savent le
faire et très vite.

Meta-Press.es c'est donc ça, un outil qui sait interroger de nombreuses
sources puis trier les résultats par ordre chronologique, le tout depuis votre
navigateur. Sans aucun autre intermédiaire entre vos journaux et vous. Vos
requêtes ne passent pas par les serveurs de Meta-Press.es, je ne suis pas
entrain de vous dire, la main sur le cœur : « faîtes moi confiance, je
n'espionne pas ce qui passe par chez moi » ; ce que je suis entrain de dire
c'est que seul les sources savent que vous leur avez demandé quelque chose, vos
requêtes ne passent pas par mes serveurs, il n'y a pas besoin d'autres
serveurs. Et ça tout le monde peut le vérifier (il suffit d'ouvrir l'outil de
Firefox qui liste les requêtes… touche F12, onglet "Réseau").

D'ailleurs en ne chargeant ni les publicités, ni les boutons de réseaux
sociaux des pages de résultats des journaux, aucun tracker, aucun mécanisme de
suivi de vos navigation n'est activé par l'utilisation de Meta-Press.es

Et pour couronner le tout, il est possible d'utiliser Meta-Press.es via le
navigateur web TorBrowser afin de ne laisser aucune trace (pas même son
adresse IP au journal consulté). C'est entre autre pour ça que le TorBrowser a
été conçu, pour permettre de lire à nouveau la presse sans personne au dessus
de votre épaule, pas même (et surtout pas même) l'éditeur du journal en
question. (bon, la semaine dernière ça marchait encore via le TorBrowser, et
ils ont profité que je sois loin du clavier pendant une semaine pour modifier
quelque chose dans le système de gestion des permissions, donc il faudra être
patient pour utiliser Meta-Press.es sur TorBrowser a priori).

Ça fonctionne également sur Android via Icecat Mobile, disponible sur
F-Droid.org.

Contrairement à un agrégateur "classique" de contenu en ligne qui vit en
intercalant de la pub entre les résultats qu'il "sélectionne" pour vous,
Meta-Press.es vous permet de choisir très précisément les sources dans
lesquelles vous souhaitez chercher. Meta-Press.es redonne à l'utilisateur le
contrôle des sources dans lesquelles il veut chercher.
