��          <               \   n   ]   P   �   J     �  h  �     R   �  `   �   Discover millions of results within seconds and explore the last ones in <b>Firefox</b> via this <b>addon</b>. Explore the press with no middlemen between the newspapers and your web browser. Schedule searches, select your press review and export it in a few clicks. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: siltaar@acoeuro.com
POT-Creation-Date: 2017-12-21 20:58+0100
PO-Revision-Date: 2017-12-21 20:59+0100
Last-Translator: Simon Descarpentries <siltaar@acoeuro.com>
Language: fr
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.0
 Découvrez des millions de résultats en quelques secondes et explorez les plus récents dans votre navigateur grâce à cette <b>extension de Firefox</b>. Explorez la presse sans intermédiaire entre vos journaux et votre navigateur web. Programmez des recherches, sélectionnez votre revue de presse et exportez-la en quelques clics. 