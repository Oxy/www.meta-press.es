= [LinuxFR.org] Meta-Press.es : un m&eacute;ta-moteur de recherche pour la presse dans votre navigateur
:slug: first_revue_in_linuxfr
:lang: fr
:date: 2019-11-01
:author: Siltaar

https://linuxfr.org/news/meta-press-es-un-meta-moteur-de-recherche-pour-la-presse-dans-votre-navigateur[*Meta‑Press.es : un méta‑moteur de recherche pour la presse dans votre navigateur*]

Mozilla vient de valider l’ajout de l’extension Meta‑Press.es à son catalogue. C’est l’aboutissement de plusieurs années d’efforts et c’est une étape importante pour ce projet de méta‑moteur de recherche, conçu d’abord pour les journalistes et *les revues de presse des associations*. […]

https://linuxfr.org/news/meta-press-es-un-meta-moteur-de-recherche-pour-la-presse-dans-votre-navigateur
